# Atlassian Exercise

## Getting started

Inside project run following commands
```bash
yarn # if you don't have yarn, install with: npm install -g yarn
yarn run start # frontend
yarn run api # backend
```
