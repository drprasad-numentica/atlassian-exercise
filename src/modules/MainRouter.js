import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { Router, Route, browserHistory } from 'react-router';
import App from './App';
import SpaceDetail from '../pages/SpaceDetail';

class Redirect extends Component {
  static contextTypes = {
    router: PropTypes.object,
  };

  // componentWillMount() {
  //   this.context.router.push('/')
  // }
  render() { return null }
}

export default class MainRouter extends Component {
  render() {
    return (
      <Router history={browserHistory}>
        <Route component={App}>
          <Route path="/" component={SpaceDetail} />
          <Route path="/spaceexplorer/:id" component={SpaceDetail} />
        </Route>
      </Router>
    );
  }
}

MainRouter.childContextTypes = {
  navOpenState: PropTypes.object,
}
