import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Page from '@atlaskit/page';
import '@atlaskit/css-reset';

import Navigation from '../components/Navigation';
import ErrorPage from '../pages/ErrorPage';
import { fetcher } from '../utils/fetcher';

export default class App extends Component {
  state = {
    spaces: {},
    error: false,
  };

  static contextTypes = {
    navOpenState: PropTypes.object,
    router: PropTypes.object,
  };

  static propTypes = {
    navOpenState: PropTypes.object,
    onNavResize: PropTypes.func,
  };

  static childContextTypes = {
    spaces: PropTypes.object,
  }

  getChildContext() {
    return {
      spaces: this.state.spaces,
    };
  }

  componentDidMount() {
    fetcher('/space')
      .then((response) => {
        if (!response.error) {
          const spacesList = (response.data || {}).items || [];
          const normalizedSpaces = spacesList.reduce((acc, space) => {
            acc[space.sys.id] = space;
            return acc;
          }, {});
          this.setState({
            spaces: normalizedSpaces,
            activeSpaceId: ((spacesList[0] || {}).sys || {}).id,
          });
        } else {
          this.setState({
            error: true,
          });
        }
      });
  }

  render() {
    const { spaces, error } = this.state;

    const navLinks = Object.keys(spaces).map((spaceId) => ({
      url: `/spaceexplorer/${spaceId}`,
      title: spaces[spaceId].fields.title,
    }));

    if (error) {
      return (
        <Page
          navigationWidth={300}
          navigation={<Navigation navLinks={navLinks} />}
        >
          <ErrorPage />
        </Page>
      );
    }
    return (
      <Page
        navigationWidth={300}
        navigation={<Navigation navLinks={navLinks} />}
      >
        {this.props.children}
      </Page>
    );
  }
}
