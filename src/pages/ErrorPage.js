
import React, { Component } from 'react';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';

export default class ErrorPage extends Component {
  render() {
    return (
      <ContentWrapper>
        <PageTitle>Oops..</PageTitle>
        <div>Something went wrong. try again.</div>
      </ContentWrapper>
    );
  }
}
