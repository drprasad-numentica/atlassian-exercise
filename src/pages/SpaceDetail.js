import PropTypes from 'prop-types';
import React, { Component } from 'react';
import Description from '../components/SpaceDescription';
import ContentWrapper from '../components/ContentWrapper';
import PageTitle from '../components/PageTitle';
import TabSection from '../components/TabSection';
import { fetcher } from '../utils/fetcher';
import ErrorPage from './ErrorPage';

export default class SpaceDetail extends Component {
  static contextTypes = {
    spaces: PropTypes.object,
    router: PropTypes.object,
  };

  constructor(props) {
    super(props);
    this.state = {
      space: null,
      assets: [],
      entries: [],
      users: {},
      error: false,
      loading: false,

      sortItem: null,
      sortKey: 'title',
      sortOrder: 'ASC',
    };
  }

  handleSort = (itemName) => ({ key, item, sortOrder }) => {

    this.setState({
      sortItem: itemName,
      sortKey: key,
      sortOrder,
    });
  }

  sort = (itemName, key, sortOrder) => {
    const items = [...this.state[itemName]];
    if (key === 'lastUpdated') {
      items.sort((a, b) => {
        if (sortOrder === 'ASC') return a[key] - b[key];
        if (sortOrder === 'DESC') return b[key] - a[key];
      });
    } else {
      items.sort((a, b) => {
        if (sortOrder === 'ASC') return a[key].localeCompare(b[key]);
        if (sortOrder === 'DESC') return b[key].localeCompare(a[key]);
      });
    }
    return items;
  }

  componentDidUpdate() {
    const spaceId = this.context.router.params.id;
    const { spaces } = this.context;

    if (!spaceId && Object.keys(spaces).length > 0) {
      this.context.router.push(`/spaceexplorer/${Object.keys(spaces)[0]}`)
    }

    const activeSpaceId = this.state.space && this.state.space.sys.id;
    console.log('this.state.space :', this.state.space);
    if (spaceId && (spaceId !== activeSpaceId)) {
      console.log('spaces[spaceId] :', spaces[spaceId]);
      this.setState({
        space: spaces[spaceId] || null,
      });
      fetcher('/users')
        .then((response) => {
          if (!response.error) {

            const users = response.data.items || [];
            const normalizedUsers = users.reduce((acc, user) => {
              acc[user.sys.id] = user;
              return acc;
            }, {});


            fetcher(`/space/${spaceId}/entries`)
              .then((response) => {
                if (!response.error) {
                  const entries = (response.data.items || []).filter((entries) => entries.sys.space === spaceId);
                  const normalizedEntries = entries.map((entry) => {
                    const updatedAt = new Date(entry.sys.updatedAt);
                    return ({
                      id: entry.sys.id,
                      title: entry.fields.title,
                      summary: entry.fields.summary,
                      createdBy: normalizedUsers[entry.sys.createdBy] ? normalizedUsers[entry.sys.createdBy].fields.name : 'Unknown',
                      updatedBy: normalizedUsers[entry.sys.updatedBy] ? normalizedUsers[entry.sys.updatedBy].fields.name : 'Unknown',
                      updatedAt,
                    })
                  });
                  this.setState({
                    entries: normalizedEntries,
                  });
                } else {
                  this.setState({ error: true });
                }
              });
            fetcher(`/space/${spaceId}/assets`)
              .then((response) => {
                if (!response.error) {
                  const assets = (response.data.items || []).filter((asset) => asset.sys.space === spaceId);
                  const normalizedAssets = assets.map((asset) => {
                    const updatedAt = new Date(asset.sys.updatedAt);
                    return ({
                      id: asset.sys.id,
                      title: asset.fields.title,
                      contentType: asset.fields.contentType,
                      fileName: asset.fields.fileName,
                      createdBy: normalizedUsers[asset.sys.createdBy] ? normalizedUsers[asset.sys.createdBy].fields.name : 'Unknown',
                      updatedBy: normalizedUsers[asset.sys.updatedBy] ? normalizedUsers[asset.sys.updatedBy].fields.name : 'Unknown',
                      updatedAt,
                    })
                  });
                  this.setState({
                    assets: normalizedAssets,
                  });
                } else {
                  this.setState({ error: true });
                }
              });
          } else {
            this.setState({
              error: true,
            })
          }
        })
    }
  }

  render() {
    const { space } = this.state;
    const { sortItem, sortKey, sortOrder, error } = this.state;
    if (!space) {
      return <ContentWrapper>No space found.</ContentWrapper>;
    }

    if(error) {
      return <ErrorPage />
    }

    const entries = sortItem === 'entries' ? this.sort('entries', sortKey, sortOrder) : this.state.entries;
    const assets = sortItem === 'assets' ? this.sort('assets', sortKey, sortOrder) : this.state.assets;
    return (
      <ContentWrapper>
        <PageTitle>{space.fields.title}</PageTitle>
        <Description description={space.fields.description} />
        <TabSection assets={assets} entries={entries} onSort={this.handleSort} />
      </ContentWrapper>
    );
  }
}
