const winston = require('winston');

const logger = winston.createLogger({
  transports: [
    new winston.transports.Console({
      json: false,
      colorize: true,
    }),
  ],
});

const apiEndpoint = 'http://localhost:4000';
const RESPONSE_TIMEOUT = 15 * 1000;

const fetcher = (url, options = {}) => {
  const finalUrl = apiEndpoint + url;
  logger.info(`Making request to ${finalUrl}`);

  let timer = null;
  const timeout = new Promise((resolve) => {
    timer = setTimeout(() => {
      logger.error(new Error('connection timeout!'));
      resolve({
        data: {}, headers: {}, statusCode: -1, error: true, message: 'connection timeout!',
      });
    }, RESPONSE_TIMEOUT);
  });

  const service = new Promise((resolve) => {
    fetch(finalUrl, options)
      .then((response) => {
        logger.log(`${response.ok ? 'info' : 'error'}`, `API response status: ${response.status}`);
        clearTimeout(timer);
        const statusCode = response.status;
        const { headers } = response;

        response.json()
          .then((data) => {
            if (statusCode >= 200 && statusCode <= 301) {
              resolve({
                data, headers, statusCode, error: false, message: null,
              });
            } else {
              logger.error(`${data.error || response.statusText}`);
              resolve({
                data,
                headers,
                statusCode,
                error: true,
                message: data.error || response.statusText,
              });
            }
          })
          .catch((error) => {
            logger.error(error);
            resolve({
              data: {}, headers, statusCode, error: true, message: error.message,
            });
          });
      })
      .catch((error) => {
        clearTimeout(timer);
        logger.error(error);
        resolve({
          data: {}, headers: {}, statusCode: -1, error: true, message: error.message,
        });
      });
  });

  return Promise.race([timeout, service]);
};

module.exports.fetcher = fetcher;
