import React from 'react';

export default ({ description }) => (
  <section style={{marginBottom: '10px'}}>
    <p>{description}</p>
  </section>
);
