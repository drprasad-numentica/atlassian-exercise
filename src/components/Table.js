import React, { Component } from 'react';
import styled from 'styled-components';
import DynamicTable from '@atlaskit/dynamic-table';

const Wrapper = styled.div`
  min-width: 600px;
`;

export default class extends Component {
  render() {
    const { head, rows, onSort } = this.props;
    return (
      <Wrapper>
        <DynamicTable
          head={head}
          rows={rows}
          rowsPerPage={10}
          defaultPage={1}
          loadingSpinnerSize="large"
          isLoading={false}
          defaultSortKey="title"
          defaultSortOrder="ASC"
          onSort={onSort}
          onSetPage={() => console.log('onSetPage')}
        />
      </Wrapper>
    );
  }
}
