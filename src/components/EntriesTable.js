import React, { Component } from 'react';
import Table from './Table';

const createHead = () => {
  return {
    cells: [
      {
        key: 'title',
        content: 'Title',
        isSortable: true,
        width: 20,
      },
      {
        key: 'summary',
        content: 'Summary',
        shouldTruncate: true,
        isSortable: true,
        width: 25,
      },
      {
        key: 'createdBy',
        content: 'Created By',
        shouldTruncate: true,
        isSortable: true,
        width: 10,
      },
      {
        key: 'updatedBy',
        content: 'Updated By',
        shouldTruncate: true,
        isSortable: true,
      },
      {
        key: 'lastUpdated',
        content: 'Last Updated',
        shouldTruncate: true,
        isSortable: true,
      },
    ],
  };
};

export default class extends Component {
  render() {
    const head = createHead();
    const rows = this.props.entries.map((entry) => {
      return ({
        cells: [
          {
            key: 'title',
            content: entry.title,
          },
          {
            key: 'summary',
            content: entry.summary,
          },
          {
            key: 'createdBy',
            content: entry.createdBy,
          },
          {
            key: 'updatedBy',
            content: entry.updatedBy,
          },
          {
            key: 'lastUpdated',
            content: `${entry.updatedAt.getMonth()}-${entry.updatedAt.getDate()}-${entry.updatedAt.getFullYear()}`,
          },
        ],
        key: entry.id,
      })
    });
    return (
      <Table head={head} rows={rows} onSort={this.props.onSort} />
    );
  }
}
