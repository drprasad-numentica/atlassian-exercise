import React, { Component } from 'react';
import Table from './Table';

const createHead = (assets) => {
  return {
    cells: [
      {
        key: 'title',
        content: 'Title',
        isSortable: true,
        width: 20,
      },
      {
        key: 'contentType',
        content: 'Content Type',
        shouldTruncate: true,
        isSortable: true,
        width: 25,
      },
      {
        key: 'fileName',
        content: 'File Name',
        shouldTruncate: true,
        isSortable: true,
        width: 25,
      },
      {
        key: 'createdBy',
        content: 'Created By',
        shouldTruncate: true,
        isSortable: true,
        width: 10,
      },
      {
        key: 'updatedBy',
        content: 'Updated By',
        shouldTruncate: true,
        isSortable: true,
      },
      {
        key: 'lastUpdated',
        content: 'Last Updated',
        shouldTruncate: true,
        isSortable: true,
      },
    ],
  };
};

export default class extends Component {
  render() {
    const head = createHead(this.props.assets);
    const rows = this.props.assets.map((asset) => {
      return ({
        cells: [
          {
            key: 'title',
            content: asset.title,
          },
          {
            key: 'contentType',
            content: asset.contentType,
          },
          {
            key: 'fileName',
            content: asset.fileName,
          },
          {
            key: 'createdBy',
            content: asset.createdBy,
          },
          {
            key: 'updatedBy',
            content: asset.updatedBy,
          },
          {
            key: 'lastUpdated',
            content: `${asset.updatedAt.getMonth()}-${asset.updatedAt.getDate()}-${asset.updatedAt.getFullYear()}`,
          },
        ],
        key: asset.id,
      })
    });
    return (
      <Table head={head} rows={rows} onSort={this.handleSort} />
    );
  }
}
