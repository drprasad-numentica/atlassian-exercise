import PropTypes from 'prop-types';
import React from 'react';
import { Link } from 'react-router';
import Nav, {
  AkContainerTitle,
  AkNavigationItem,
} from '@atlaskit/navigation';
import atlaskitLogo from '../images/atlaskit.png';



export default class Navigation extends React.Component {

  static contextTypes = {
    router: PropTypes.object,
  };

  render() {
    return (
      <Nav
        isOpen={true}
        width={304}
        containerHeaderComponent={() => (
          <AkContainerTitle
            href="/"
            icon={
              <img alt="spaceexplorer logo" src={atlaskitLogo} />
            }
            text="Space Explorer"
          />
        )}
      >
        {
          this.props.navLinks.map(link => {
            const { url, title } = link;
            return (
              <Link key={url} to={url}>
                <AkNavigationItem
                  text={title}
                  isSelected={this.context.router.isActive(url, true)}
                />
              </Link>
            );
          }, this)
        }
      </Nav>
    );
  }
}
