import React from 'react';
import Tooltip from '@atlaskit/tooltip';
import Tabs, { TabItem } from '@atlaskit/tabs';
import EntriesTable from './EntriesTable';
import AssetTable from './AssetTable';

/** This custom component wraps a tooltip around the tab item */
const TooltipItem = (props) => (
  <Tooltip content={props.data.tooltip}>
    <TabItem {...props} />
  </Tooltip>
);

export default ({ entries, assets, onSort }) => {
  const tabs = [
    {
      label: 'Entries',
      content: <EntriesTable entries={entries} onSort={onSort('entries')} />,
      tooltip: 'Entries',
      href: '#',
    },
    {
      label: 'Assets',
      content: <AssetTable assets={assets} onSort={onSort('assets')} />,
      tooltip: 'Assets',
      href: '#',
    },
  ];
  return(
    <div>
      <Tabs
        components={{ Item: TooltipItem }}
        onSelect={(tab, index) => console.log('Selected Tab', index + 1)}
        tabs={tabs}
      />
    </div>
)};
